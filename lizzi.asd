(asdf:defsystem lizzi
	:description "Lizzi converts a lisp program to an image file and then can run that image file as if it were a lisp program."
	:author "David A. Redick <tinyweldingtorch@gmail.com>"
	:version "0.0"
	:license "GNU GPL v3"
	:licence "GNU GPL v3"
	:depends-on (opticl)
	:components (
		(:module source
			:serial t
			:components (
				(:file "lizzi")
				(:file "compile")
				(:file "run")
			)
		)
	)
)