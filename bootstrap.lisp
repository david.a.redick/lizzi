(princ "Defining system: asdf")
(fresh-line)
(ext:cd "../asdf")
(load "asdf.lisp")
(load "asdf.asd")

(princ "Defining system: alexandria")
(fresh-line)
(ext:cd "../alexandria")
(load "alexandria.asd")

(princ "Defining system: babel")
(fresh-line)
(ext:cd "../babel_0.3.0")
(load "babel.asd")

(princ "Defining system: chipz")
(fresh-line)
(ext:cd "../chipz_0.7.4")
(load "chipz.asd")

(princ "Defining system: clem")
(fresh-line)
(ext:cd "../clem")
(load "clem.asd")

(princ "Defining system: cl-jpeg")
(fresh-line)
(ext:cd "../cl-jpeg")
(load "cl-jpeg.asd")

(princ "Defining system: ieee-floats")
(fresh-line)
(ext:cd "../ieee-floats")
(load "ieee-floats.asd")

(princ "Defining system: iterate")
(fresh-line)
(ext:cd "../iterate-1.4.3")
(load "iterate.asd")

(princ "Defining system: monkeylib-binary-data")
(fresh-line)
(ext:cd "../monkeylib-binary-data")
(load "com.gigamonkeys.binary-data.asd")

(princ "Defining system: opticl")
(fresh-line)
(ext:cd "../opticl")
(load "opticl.asd")

(princ "Defining system: png-read")
(fresh-line)
(ext:cd "../png-read")
(load "png-read.asd")

(princ "Defining system: retrospectiff")
(fresh-line)
(ext:cd "../retrospectiff")
(load "retrospectiff.asd")

(princ "Defining system: salza2")
(fresh-line)
(ext:cd "../salza2-2.0.7")
(load "salza2.asd")

(princ "Defining system: skippy")
(fresh-line)
(ext:cd "../skippy-1.3.7")
(load "skippy.asd")

(princ "Defining system: trivial-features")
(fresh-line)
(ext:cd "../trivial-features_0.6")
(load "trivial-features.asd")

(princ "Defining system: zpng")
(fresh-line)
(ext:cd "../zpng-1.2")
(load "zpng.asd")

(princ "Defining system: lizzi")
(fresh-line)
(ext:cd "../lizzi")
(load "lizzi.asd")

(princ "Loading system: lizzi")
(fresh-line)
(asdf:load-system 'lizzi)
