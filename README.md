# What is Lizzi?

Lizzi is an art and computer science experiment inspired by the puzzle [Izzi](http://www.thinkfun.com/izzi) and [Lisp](http://en.wikipedia.org/wiki/Lisp).
Lizzi uses specialized pictures (similiar in appearence to a [HCCB](http://en.wikipedia.org/wiki/High_Capacity_Color_Barcode)) to encode a program.

More specifically, Lizzi converts a lisp program to an image file and then can run that image file as if it were a lisp program.

# How to Use

The following steps will provide a fully loaded [clisp](http://www.gnu.org/software/clisp) shell:

	$ cd ~
	$ mkdir projects-lizzi
	$ cd projects-lizzi
	$ git clone http://github.com/david-redick/lizzi.git
	$ cd lizzi
	$ ./get.dependencies.sh
	$ ./setup.sh

Then:

	clisp> (lizzi:lizzi-run "examples/beer.png")
or

	clisp> (lizzi:lizzi-compile "examples/hello.lisp" "examples/hello.png")
	clisp> (lizzi:lizzi-run "examples/hello.png")

# Dependencies

Lizzi requires several Common Lisp libraries to read and write [png](http://en.wikipedia.org/wiki/Portable_Network_Graphics) files.
Most of these are availible via [quicklisp](http://www.quicklisp.org) but due to out of date releases and bugs, I recommend using the bundled `get.dependencies.sh` and `setup.sh` scripts.

	alexandria                      http://common-lisp.net/project/alexandria
	ASDF 2                          http://common-lisp.net/project/asdf
	babel                           http://common-lisp.net/project/babel
	chipz                           http://www.method-combination.net/lisp/chipz
	clem                            http://cyrusharmon.org/projects?project=clem
	cl-jpeg                         http://common-lisp.net/project/cl-jpeg
	iterate                         http://common-lisp.net/project/iterate
	ieee-floats                     http://common-lisp.net/project/ieee-floats
	monkeylib-binary-data           https://github.com/gigamonkey/monkeylib-binary-data
	opticl                          https://github.com/slyrus/opticl
	png-read                        https://github.com/Ramarren/png-read
	retrospectiff                   http://cyrusharmon.org/projects?project=retrospectiff
	salza2                          http://www.xach.com/lisp/salza2
	skippy                          http://www.xach.com/lisp/skippy
	trivial-features                http://www.cliki.net/trivial-features
	zpng                            http://www.xach.com/lisp/zpng

# Tested On

GNU + Linux with [clisp](http://www.gnu.org/software/clisp)
Windows XP running [Cygwin](http://www.cygwin.com) with [clisp](http://www.gnu.org/software/clisp)

# Project

Copyright 2011 by David A. Redick

Released under the [GNU General Public License version 3](http://www.gnu.org/licenses/gpl.html).

Project Home: https://github.com/david-redick/lizzi

Maintainer: David A. Redick tinyweldingtorch@gmail.com