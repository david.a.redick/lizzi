(defpackage :lizzi
	(:export :lizzi-compile)
)

(in-package :lizzi)

; Won't compile empty file.
(defun lizzi-compile (file-lisp file-png)
	(with-open-file (stream file-lisp)
		(let*
			(
				; number of chars in file
				(num-chars (file-length stream))
				; determine rect size (width and/or height) in tiles
				(image-size-in-tiles (ceiling (sqrt num-chars)))
				(image-size-in-pixels (* *lizzi-tile-size* image-size-in-tiles))
				(img (opticl:make-8-bit-rgb-image image-size-in-pixels image-size-in-pixels))
				(char-index 0)
				(visit-func
					(lambda (char)
						(draw-tile
							img
							(make-tile-image-from-code (char-code char))
							char-index
							image-size-in-tiles
						)
						(incf char-index)
					)
				)
			)
			; clear the image
			(opticl:fill-image img 255 255 255)
			; for each char get tile and blit on to primary image
			(call-for-each-char-in-file stream visit-func)
			(opticl:write-png-file file-png img)
		)
	)
)

(defun call-for-each-char-in-file (stream func)
	(do
		(
			(char (read-char stream nil) (read-char stream nil))
		)
		((null char))
		(funcall func char)
	)
)

(defun make-tile-image-from-code (code)
	(let*
		(
			(height *lizzi-tile-size*)
			(width *lizzi-tile-size*)
			(img (opticl:make-8-bit-rgb-image height width))
			(is-bit-0 (logbitp 0 code))
			(is-bit-1 (logbitp 1 code))
			(is-bit-2 (logbitp 2 code))
			(is-bit-3 (logbitp 3 code))
			(is-bit-4 (logbitp 4 code))
			(is-bit-5 (logbitp 5 code))
			(is-bit-6 (logbitp 6 code))
			(is-bit-7 (logbitp 7 code))
		)
		(opticl:fill-image img 255 255 255)

		; There is a bug in opticl that makes it impossible to draw a line with no slope.
		; Also there is no fill-triangle method.
		; So we have to draw the stupid lines one at a time.
		(dotimes (i 17)
			                                     ; y      x    y        x    color-rgb
			; 1 (bit 0)
			(if is-bit-0 (opticl:draw-line img (- 16 i) 16 (- 16 i) (+ 16 i) 0 0 0) )
			; 2 (bit 1)
			(if is-bit-1 (opticl:draw-line img (- 16 i) 32 (- 16 i) (+ 16 i) 0 0 0) )
			; 4 (bit 2)
			(if is-bit-2 (opticl:draw-line img (+ 16 i) 32 (+ 16 i) (+ 16 i) 0 0 0) )
			; 8 (bit 3)
			(if is-bit-3 (opticl:draw-line img (+ 16 i) 16 (+ 16 i) (+ 16 i) 0 0 0) )
			; 16 (bit 4)
			(if is-bit-4 (opticl:draw-line img (+ 16 i) 16 (+ 16 i) (- 16 i) 0 0 0) )
			; 32 (bit 5)
			(if is-bit-5 (opticl:draw-line img (+ 16 i) 0 (+ 16 i) (- 16 i) 0 0 0) )
			; 64 (bit 6)
			(if is-bit-6 (opticl:draw-line img (- 16 i) 0 (- 16 i) (- 16 i) 0 0 0) )
			; 128 (bit 7)
			(if is-bit-7 (opticl:draw-line img i i i 16 0 0 0) )
		)
		img
	)
)

(defun draw-tile (image tile char-index image-size-in-tiles)
	(let*
		(
			(row (lizzi-quotient char-index image-size-in-tiles))
			(col (mod char-index image-size-in-tiles))
			(dest-y (* row *lizzi-tile-size*))
			(dest-x (* col *lizzi-tile-size*))
		)
		(blit image dest-y dest-x tile)
	)
)

(defun blit (image-dest dest-y dest-x image-src)
	(let
		(
			(src-max-y (array-dimension image-src 0))
			(src-max-x (array-dimension image-src 1))
		)
		(dotimes (src-y src-max-y)
			(dotimes (src-x src-max-x)
				(setf (opticl:pixel* image-dest (+ dest-y src-y) (+ dest-x src-x))
					(opticl:pixel* image-src src-y src-x)
				)
			)
		)
		image-dest
	)
)