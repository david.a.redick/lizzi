(defpackage :lizzi
	(:export :lizzi-run)
)

(in-package :lizzi)

(defun lizzi-run (file-png)
	(let*
		(
			(image (opticl:read-png-file file-png))
			; height is ok since lizzi "programs" are square.
			(image-size-in-tiles (/ (array-dimension image 0) *lizzi-tile-size*))
			; total number of tiles (chars)
			(num-chars (* image-size-in-tiles image-size-in-tiles))

		)
		; validate
		; (mod image-size-in-tiles *lizzi-tile-size*) if not zero then error
		(eval (read-from-string (with-output-to-string (stream)
			(dotimes (i num-chars)
				;(princ (get-nth-char-from-lizzi-image image i image-size-in-tiles))
				;(fresh-line)
				(write-char (get-nth-char-from-lizzi-image image i image-size-in-tiles) stream)
			)
		)))
		t
	)

;
;
	; create code from tiles
	; (dotimes (row height-in-tiles)
	;	(dotimes (col width-tiles)))
	; e.g. (append '() (list '2)) -> (2)

	; execute (eval my-code)
)

(defun get-nth-char-from-lizzi-image (image char-index image-size-in-tiles)
	(let*
		(
			(row (lizzi-quotient char-index image-size-in-tiles))
			(col (mod char-index image-size-in-tiles))
			(offset-y (* row *lizzi-tile-size*))
			(offset-x (* col *lizzi-tile-size*))
			(code-from-image (get-code-from-lizzi-image image offset-y offset-x))
		)
		(code-char (if (= 0 code-from-image) 32 code-from-image))
	)
)

(defun get-code-from-lizzi-image (image offset-y offset-x)
	(let*
		(
			; bit-0 ( 1 )
			(y0 offset-y) ;  0
			(x0 (- (+ offset-x *lizzi-tile-size*) 2)) ; 31

			; bit-1 ( 2 )
			(y1 (+ offset-y 1)) ;  1
			(x1 (- (+ offset-x *lizzi-tile-size*) 1)) ; 32

			; bit-2 ( 4 )
			(y2 (- (+ offset-y *lizzi-tile-size*) 2)) ; 31
			(x2 (- (+ offset-x *lizzi-tile-size*) 1)) ; 32

			; bit-3 ( 8 )
			(y3 (- (+ offset-y *lizzi-tile-size*) 1)) ; 32
			(x3 (- (+ offset-x *lizzi-tile-size*) 2)) ; 31

			; bit-4 ( 16 )
			(y4 (- (+ offset-y *lizzi-tile-size*) 1)) ; 32
			(x4 (+ offset-x 1)) ;  1

			; bit-5 ( 32 )
			(y5 (- (+ offset-y *lizzi-tile-size*) 2)) ; 31
			(x5 offset-x) ;  0

			; bit-6 ( 64 )
			(y6 (+ offset-y 1)) ;  1
			(x6 offset-x) ;  0

			; bit-7 ( 128 )
			(y7 offset-y) ;  0
			(x7 (+ offset-x 1)) ;  1

			; If our test pixel is black then turn bit on.
			(bit-0 (if (equal '(0 0 0) (opticl:pixel* image y0 x0))   1 0))
			(bit-1 (if (equal '(0 0 0) (opticl:pixel* image y1 x1))   2 0))
			(bit-2 (if (equal '(0 0 0) (opticl:pixel* image y2 x2))   4 0))
			(bit-3 (if (equal '(0 0 0) (opticl:pixel* image y3 x3))   8 0))
			(bit-4 (if (equal '(0 0 0) (opticl:pixel* image y4 x4))  16 0))
			(bit-5 (if (equal '(0 0 0) (opticl:pixel* image y5 x5))  32 0))
			(bit-6 (if (equal '(0 0 0) (opticl:pixel* image y6 x6))  64 0))
			(bit-7 (if (equal '(0 0 0) (opticl:pixel* image y7 x7)) 128 0))
		)
		;(princ x0) (princ " ") (princ y0) (princ " = ") (princ bit-0)
		;(fresh-line)
		;(princ x1) (princ " ") (princ y1) (princ " = ") (princ bit-1)
		;(fresh-line)
		;(princ x2) (princ " ") (princ y2) (princ " = ") (princ bit-2)
		;(fresh-line)
		;(princ x3) (princ " ") (princ y3) (princ " = ") (princ bit-3)
		;(fresh-line)
		;(princ x4) (princ " ") (princ y4) (princ " = ") (princ bit-4)
		;(fresh-line)
		;(princ x5) (princ " ") (princ y5) (princ " = ") (princ bit-5)
		;(fresh-line)
		;(princ x6) (princ " ") (princ y6) (princ " = ") (princ bit-6)
		;(fresh-line)
		;(princ x7) (princ " ") (princ y7) (princ " = ") (princ bit-7)
		;(fresh-line)
		;(princ (logior bit-0 bit-1 bit-2 bit-3 bit-4 bit-5 bit-6 bit-7))
		;(fresh-line)

		(logior bit-0 bit-1 bit-2 bit-3 bit-4 bit-5 bit-6 bit-7)
	)
)