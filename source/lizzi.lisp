(defpackage :lizzi
	(:export
		:*lizzi-tile-size*
		:lizzi-quotient
	)
)

(in-package :lizzi)

(defconstant *lizzi-tile-size* 33)

; Some Lisp implementations don't have the quotient function.
(defun lizzi-quotient (x y)
	(/ (- x (mod x y)) y) 
)