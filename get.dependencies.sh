#!/bin/sh

cd ..

git clone http://common-lisp.net/r/projects/alexandria/alexandria.git
patch -p0 -i ./lizzi/patches/alexandria.patch

git clone http://common-lisp.net/projects/asdf/asdf.git

wget http://common-lisp.net/project/babel/releases/babel_0.3.0.tar.gz
tar -zxf babel_0.3.0.tar.gz
patch -p0 -i ./lizzi/patches/babel_0.3.0.patch

wget http://www.method-combination.net/lisp/files/chipz_0.7.4.tar.gz
tar -zxf chipz_0.7.4.tar.gz

git clone http://cyrusharmon.org/pub/git/clem.git

wget http://www.common-lisp.net/project/mcclim/cl-jpeg.tar.gz
tar -zxf cl-jpeg.tar.gz
patch -p0 -i ./lizzi/patches/cl-jpeg.patch

wget http://common-lisp.net/project/ieee-floats/ieee-floats.tgz
tar -zxf ieee-floats.tgz

wget http://common-lisp.net/project/iterate/releases/iterate-1.4.3.tar.gz
tar -zxf iterate-1.4.3.tar.gz
patch -p0 -i ./lizzi/patches/iterate-1.4.3.patch

git clone http://github.com/gigamonkey/monkeylib-binary-data.git

git clone http://github.com/slyrus/opticl.git
patch -p0 -i ./lizzi/patches/opticl.patch

git clone http://github.com/Ramarren/png-read.git

git clone http://git.cyrusharmon.org/pub/git/retrospectiff.git

wget http://www.xach.com/lisp/salza2.tgz
tar -zxf salza2.tgz
# There should be directory salza2-2.0.7

wget http://www.xach.com/lisp/skippy.tgz
tar -zxf skippy.tgz
# There should be directory skippy-1.3.7
patch -p0 -i ./lizzi/patches/skippy-1.3.7.patch

wget http://common-lisp.net/~loliveira/tarballs/trivial-features/trivial-features_0.6.tar.gz
tar -zxf trivial-features_0.6.tar.gz
patch -p0 -i ./lizzi/patches/trivial-features_0.6.patch

wget http://www.xach.com/lisp/zpng.tgz
tar -zxf zpng.tgz
# There should be directory zpng-1.2